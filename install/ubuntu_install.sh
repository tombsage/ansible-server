#!/bin/bash

# APT UPDATE
sudo apt update

# INSTALL ANSIBLE
sudo apt install ansible -y

# INSTALL GIT
sudo apt install git -y

# ANSIBLE INSTALL
ansible-pull -U git@bitbucket.org:tombsage/ansible-server.git playbook.yml --ask-sudo-pass
